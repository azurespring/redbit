<?php
/**
 * JSONRPCException.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit;

/**
 * An object describing the error if one occurred, otherwise null.
 */
class JSONRPCException extends \RuntimeException
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param int $code
     *
     * @return $this
     */
    public function setCode(int $code)
    {
        $this->code = $code;

        return $this;
    }
}
