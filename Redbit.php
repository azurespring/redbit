<?php
/**
 * Redbit.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */

namespace AzureSpring\Redbit;

use Psr\Log\LoggerInterface;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Reduced bitcoin.
 */
class Redbit implements RedbitInterface, PropertyTypeExtractorInterface
{
    private $client;

    private $byPosition;

    private $logger;

    private $serializer;

    private $resultClass;


    /**
     * Constructor.
     *
     * @param \GuzzleHttp\Client $client
     * @param bool               $byPosition JSON-RPC params through an Array (true) or
     *                                       by-name through an Object (false, default)
     * @param LoggerInterface    $logger
     */
    public function __construct(\GuzzleHttp\Client $client, bool $byPosition = false, ?LoggerInterface $logger = null)
    {
        $phpDocExtractor = new PhpDocExtractor();
        $reflectionExtractor = new ReflectionExtractor();

        $normalizer = new ObjectNormalizer(
            null,
            new IsPrefixNameConverter(),
            null,
            new PropertyInfoExtractor(
                [$reflectionExtractor],
                [$this, $phpDocExtractor, $reflectionExtractor],
                [$phpDocExtractor, $reflectionExtractor],
                [$reflectionExtractor]
            )
        );

        $this->client = $client;
        $this->byPosition = $byPosition;
        $this->logger = $logger;
        $this->serializer = new Serializer([$normalizer, new ArrayDenormalizer()], [new JsonEncoder()]);
    }

    /**
     * {@inheritDoc}
     */
    public function walletLock()
    {
        $this->remoteCall('walletlock');

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function walletPassphrase(string $passphrase, int $timeout)
    {
        $this->remoteCall('walletpassphrase', ['passphrase' => $passphrase, 'timeout' => $timeout], null, ['passphrase']);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAddress(string $address): Result\ValidateAddressResult
    {
        return $this->remoteCall('validateaddress', ['address' => $address], Result\ValidateAddressResult::class);
    }

    /**
     * {@inheritDoc}
     */
    public function listSinceBlock(string $hash = '', int $confirmations = 1, bool $watchOnly = false): Result\ListSinceBlockResult
    {
        return $this->remoteCall(
            'listsinceblock',
            [
                'blockhash' => $hash,
                'target_confirmations' => $confirmations,
                'include_watchonly' => $watchOnly,
            ],
            Result\ListSinceBlockResult::class
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getTransaction(string $txId): Result\Transaction
    {
        return $this->remoteCall('gettransaction', ['txid' => $txId], Result\Transaction::class);
    }

    /**
     * {@inheritDoc}
     */
    public function getBalance(string $account = '*'): string
    {
        return $this->remoteCall('getbalance', ['account' => $account]);
    }

    /**
     * {@inheritDoc}
     */
    public function listUnspent(int $minconf = 1, int $maxconf = 9999999, ?array $addresses = null): array
    {
        return $this->remoteCall(
            'listunspent',
            [
                'minconf' => $minconf,
                'maxconf' => $maxconf,
                'addresses' => $addresses,
            ],
            Result\UnspentOutput::class.'[]'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function estimateFee(int $blocks): float
    {
        return $this->remoteCall('estimatefee', ['nblocks' => $blocks]);
    }

    /**
     * {@inheritDoc}
     */
    public function getNewAddress(string $account = ''): string
    {
        return $this->remoteCall('getnewaddress', ['account' => $account]);
    }

    /**
     * {@inheritDoc}
     */
    public function dumpPrivKey(string $address): string
    {
        return $this->remoteCall('dumpprivkey', ['address' => $address]);
    }

    /**
     * {@inheritDoc}
     */
    public function importPrivKey(string $privKey, string $account = '', bool $rescan = true)
    {
        $this->remoteCall('importprivkey', [
            'privkey' => $privKey,
            'label' => $account,
            'rescan' => $rescan,
        ]);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function createRawTransaction(array $inputs, array $outputs, int $locktime = null): string
    {
        return $this->remoteCall('createrawtransaction', [
            'inputs' => $inputs,
            'outputs' => (object) $outputs,
            'locktime' => $locktime,
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function fundRawTransaction(string $hex, ?Parameter\FundRawTransactionOptions $options = null): Result\FundRawTransactionResult
    {
        $params = ['hexstring' => $hex];
        if ($options) {
            $params['options'] = $options;
        }

        return $this->remoteCall('fundrawtransaction', $params, Result\FundRawTransactionResult::class);
    }

    /**
     * {@inheritDoc}
     */
    public function signRawTransaction(string $hex, ?array $prevTxs = null, ?array $privKeys = null, ?string $sighashtype = null): Result\SignRawTransactionResult
    {
        return $this->remoteCall(
            'signrawtransaction',
            [
                'hexstring' => $hex,
                'prevtxs' => $prevTxs,
                'privkeys' => $privKeys,
                'sighashtype' => $sighashtype,
            ],
            Result\SignRawTransactionResult::class
        );
    }

    /**
     * {@inheritDoc}
     */
    public function sendRawTransaction(string $transaction, bool $allowHighFees = false): string
    {
        return $this->remoteCall('sendrawtransaction', ['hexstring' => $transaction, 'allowhighfees' => $allowHighFees]);
    }

    /**
     * {@inheritDoc}
     */
    public function decodeRawTransaction(string $transaction): Result\DecodeRawTransactionResult
    {
        return $this->remoteCall('decoderawtransaction', ['hexstring' => $transaction], Result\DecodeRawTransactionResult::class);
    }

    /**
     * {@inheritDoc}
     */
    public function getTypes($class, $property, array $context = array())
    {
        switch ($property) {
            case 'result':
                if ($this->resultClass) {
                    return [new Type(Type::BUILTIN_TYPE_OBJECT, true, $this->resultClass)];
                }
                /* fall through */

            default:
                return null;
        }
    }

    /**
     * JSON-RPC
     *
     * @param string      $method
     * @param array       $params
     * @param string|null $class
     * @param string[]    $blackOut
     *
     * @return mixed
     */
    protected function remoteCall(string $method, array $params = [], ?string $class = null, array $blackOut = [])
    {
        if ($this->logger) {
            $this->logger->debug('remoteCall', [
                'method' => $method,
                'params' => json_encode(
                    $this->byPosition
                    ? array_values($this->sanitize($params, $blackOut))
                    : (object) $this->sanitize($params, $blackOut)
                ),
                'class' => $class,
            ]);
        }

        /**
         * @var JSONRPCResponse $res
         */
        $this->resultClass = $class;
        $res = $this->serializer->deserialize(
            $this
                ->client
                ->post('', [
                    'headers' => [
                        'Content-Type' => 'text/plain',
                    ],
                    'json' => [
                        'method' => $method,
                        'params' => $this->byPosition ? array_values($params) : (object) $params,
                    ],
                ])
                ->getBody()
                ->getContents(),
            JSONRPCResponse::class,
            'json'
        );
        if ($res->getError()) {
            throw $res->getError();
        }

        return $res->getResult();
    }

    /**
     * @param array $params
     * @param array $blackOut
     *
     * @return array
     */
    private function sanitize(array $params, array $blackOut): array
    {
        return array_combine(
            array_keys($params),
            array_map(
                function ($v, $k) use ($blackOut) {
                    return in_array($k, $blackOut)
                        ? str_repeat('*', strlen($v))
                        : $v;
                },
                $params,
                array_keys($params)
            )
        );
    }
}
