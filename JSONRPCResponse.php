<?php
/**
 * JSONRPCResponse.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */

namespace AzureSpring\Redbit;

/**
 * JSON-RPC Response
 */
class JSONRPCResponse
{
    /**
     * @var string|null
     */
    private $id;

    /**
     * @var JSONRPCException|null
     */
    private $error;

    /**
     * @var mixed
     */
    private $result;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     *
     * @return $this
     */
    public function setId(string $id = null): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return JSONRPCException
     */
    public function getError(): ?JSONRPCException
    {
        return $this->error;
    }

    /**
     * @param JSONRPCException|null $error
     *
     * @return $this
     */
    public function setError(JSONRPCException $error = null): self
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     *
     * @return $this
     */
    public function setResult($result): self
    {
        $this->result = $result;

        return $this;
    }
}
