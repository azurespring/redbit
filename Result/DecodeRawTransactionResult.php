<?php
/**
 * DecodeRawTransactionResult.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Result;

/**
 * The decoded transaction
 */
class DecodeRawTransactionResult
{
    /**
     * The transaction id
     *
     * @var string
     */
    private $txid;

    /**
     * The transaction hash (differs from txid for witness transactions)
     *
     * @var string
     */
    private $hash;

    /**
     * The transaction size
     *
     * @var int
     */
    private $size;

    /**
     * The virtual transaction size (differs from size for witness transactions)
     *
     * @var int
     */
    private $vsize;

    /**
     * The version
     *
     * @var int
     */
    private $version;

    /**
     * The lock time
     *
     * @var int
     */
    private $locktime;

    /**
     * @var array
     */
    private $vin;

    /**
     * @var array
     */
    private $vout;

    /**
     * @return string
     */
    public function getTxid(): string
    {
        return $this->txid;
    }

    /**
     * @param string $txid
     *
     * @return $this
     */
    public function setTxid(string $txid): self
    {
        $this->txid = $txid;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     *
     * @return $this
     */
    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     *
     * @return $this
     */
    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return int
     */
    public function getVsize(): int
    {
        return $this->vsize;
    }

    /**
     * @param int $vsize
     *
     * @return $this
     */
    public function setVsize(int $vsize): self
    {
        $this->vsize = $vsize;

        return $this;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     *
     * @return $this
     */
    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return int
     */
    public function getLocktime(): int
    {
        return $this->locktime;
    }

    /**
     * @param int $locktime
     *
     * @return $this
     */
    public function setLocktime(int $locktime): self
    {
        $this->locktime = $locktime;

        return $this;
    }

    /**
     * @return array
     */
    public function getVin(): array
    {
        return $this->vin;
    }

    /**
     * @param array $vin
     *
     * @return $this
     */
    public function setVin(array $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    /**
     * @return array
     */
    public function getVout(): array
    {
        return $this->vout;
    }

    /**
     * @param array $vout
     *
     * @return $this
     */
    public function setVout(array $vout): self
    {
        $this->vout = $vout;

        return $this;
    }
}
