<?php
/**
 * ValidateAddressResult.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Result;

/**
 * Information about the address.
 */
class ValidateAddressResult
{
    /**
     * @var bool
     */
    private $valid;

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @param bool $valid
     *
     * @return $this
     */
    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }
}
