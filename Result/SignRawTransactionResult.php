<?php
/**
 * SignRawTransactionResult.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Result;

/**
 * The transaction with any signatures made
 */
class SignRawTransactionResult
{
    /**
     * The resulting serialized transaction encoded as hex with any signatures made inserted.
     * If no signatures were made, this will be the same transaction provided in parameter #1
     *
     * @var string
     */
    private $hex;

    /**
     * The value true if transaction is fully signed; the value false if more signatures are required
     *
     * @var bool
     */
    private $complete;

    /**
     * @return string
     */
    public function getHex(): string
    {
        return $this->hex;
    }

    /**
     * @param string $hex
     *
     * @return $this
     */
    public function setHex(string $hex): self
    {
        $this->hex = $hex;

        return $this;
    }

    /**
     * @return bool
     */
    public function isComplete(): bool
    {
        return $this->complete;
    }

    /**
     * @param bool $complete
     *
     * @return $this
     */
    public function setComplete(bool $complete): self
    {
        $this->complete = $complete;

        return $this;
    }
}
