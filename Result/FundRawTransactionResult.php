<?php
/**
 * FundRawTransactionResult.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Result;

/**
 * Information about the created transaction.
 */
class FundRawTransactionResult
{
    /**
     * The resulting unsigned raw transaction in serialized transaction format encoded as hex
     *
     * @var string
     */
    private $hex;

    /**
     * Fee in BTC the resulting transaction pays
     *
     * @var float
     */
    private $fee;

    /**
     * The position of the added change output, or -1 if no change output was added
     *
     * @var int
     */
    private $changepos;

    /**
     * @return string
     */
    public function getHex(): string
    {
        return $this->hex;
    }

    /**
     * @param string $hex
     *
     * @return $this
     */
    public function setHex(string $hex): self
    {
        $this->hex = $hex;

        return $this;
    }

    /**
     * @return float
     */
    public function getFee(): float
    {
        return $this->fee;
    }

    /**
     * @param float $fee
     *
     * @return $this
     */
    public function setFee(float $fee): self
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * @return int
     */
    public function getChangepos(): int
    {
        return $this->changepos;
    }

    /**
     * @param int $changepos
     *
     * @return $this
     */
    public function setChangepos(int $changepos): self
    {
        $this->changepos = $changepos;

        return $this;
    }
}
