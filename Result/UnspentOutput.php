<?php
/**
 * UnspentOutput.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Result;
use AzureSpring\Redbit\Parameter\Input;

/**
 * An object describing a particular unspent output belonging to this wallet
 */
class UnspentOutput implements \JsonSerializable
{
    /**
     * The TXID of the transaction containing the output, encoded as hex in RPC byte order
     *
     * @var string
     */
    private $txid;

    /**
     * The output index number (vout) of the output within its containing transaction
     *
     * @var int
     */
    private $vout;

    /**
     * The P2PKH or P2SH address the output paid. Only returned for P2PKH or P2SH output scripts
     *
     * @var string|null
     */
    private $address;

    /**
     * The output script paid, encoded as hex
     *
     * @var string
     */
    private $scriptPubKey;

    /**
     * If the output is a P2SH whose script belongs to this wallet, this is the redeem script
     *
     * @var string|null
     */
    private $redeemScript;

    /**
     * The amount paid to the output in bitcoins
     *
     * @var float
     */
    private $amount;

    /**
     * The number of confirmations received for the transaction containing this output
     *
     * @var int
     */
    private $confirmations;

    /**
     * Set to true if the private key or keys needed to spend this output are part of the wallet.
     * Set to false if not (such as for watch-only addresses)
     *
     * @var bool
     */
    private $spendable;

    /**
     * Set to true if the wallet knows how to spend this output. Set to false if the wallet
     * does not know how to spend the output. It is ignored if the private keys are available
     *
     * @var bool
     */
    private $solvable;

    /**
     * @return string
     */
    public function getTxid(): string
    {
        return $this->txid;
    }

    /**
     * @param string $txid
     *
     * @return $this
     */
    public function setTxid(string $txid): self
    {
        $this->txid = $txid;

        return $this;
    }

    /**
     * @return int
     */
    public function getVout(): int
    {
        return $this->vout;
    }

    /**
     * @param int $vout
     *
     * @return $this
     */
    public function setVout(int $vout): self
    {
        $this->vout = $vout;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param null|string $address
     *
     * @return $this
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getScriptPubKey(): string
    {
        return $this->scriptPubKey;
    }

    /**
     * @param string $scriptPubKey
     *
     * @return $this
     */
    public function setScriptPubKey(string $scriptPubKey): self
    {
        $this->scriptPubKey = $scriptPubKey;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getRedeemScript(): ?string
    {
        return $this->redeemScript;
    }

    /**
     * @param null|string $redeemScript
     *
     * @return $this
     */
    public function setRedeemScript(?string $redeemScript): self
    {
        $this->redeemScript = $redeemScript;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return int
     */
    public function getConfirmations(): int
    {
        return $this->confirmations;
    }

    /**
     * @param int $confirmations
     *
     * @return $this
     */
    public function setConfirmations(int $confirmations): self
    {
        $this->confirmations = $confirmations;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSpendable(): bool
    {
        return $this->spendable;
    }

    /**
     * @param bool $spendable
     *
     * @return $this
     */
    public function setSpendable(bool $spendable): self
    {
        $this->spendable = $spendable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSolvable(): bool
    {
        return $this->solvable;
    }

    /**
     * @param bool $solvable
     *
     * @return $this
     */
    public function setSolvable(bool $solvable): self
    {
        $this->solvable = $solvable;

        return $this;
    }

    /**
     * @return Input
     */
    public function asInput(): Input
    {
        return new Input($this->getTxid(), $this->getVout());
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return (object) array_filter(
            [
                'txid'          => $this->getTxid(),
                'vout'          => $this->getVout(),
                'address'       => $this->getAddress(),
                'scriptPubKey'  => $this->getScriptPubKey(),
                'redeemScript'  => $this->getRedeemScript(),
                'amount'        => $this->getAmount(),
                'confirmations' => $this->getConfirmations(),
                'spendable'     => $this->isSpendable(),
                'solvable'      => $this->isSolvable(),
            ],
            function ($var) {
                return null !== $var;
            }
        );
    }
}
