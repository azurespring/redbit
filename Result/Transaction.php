<?php
/**
 * Transaction.php.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Result;

/**
 * An payment which did not appear in the specified block or an earlier block
 */
class Transaction
{
    const SEND     = 'send';
    const RECEIVE  = 'receive';
    const GENERATE = 'generate';
    const IMMATURE = 'immature';
    const ORPHAN   = 'orphan';

    /**
     * Set to true if the payment involves a watch-only address. Otherwise not returned
     *
     * @var bool|null
     */
    private $involvesWatchonly;

    /**
     * The account which the payment was credited to or debited from.
     * May be an empty string (“”) for the default account
     *
     * @var string
     */
    private $account;

    /**
     * The address paid in this payment, which may be someone else’s address not belonging to this wallet.
     * May be empty if the address is unknown, such as when paying to a non-standard pubkey script
     *
     * @var string|null
     */
    private $address;

    /**
     * Set to one of the following values:
     *     send     if sending payment
     *     receive  if this wallet received payment in a regular transaction
     *     generate if a matured and spendable coinbase
     *     immature if a coinbase that is not spendable yet
     *     orphan   if a coinbase from a block that’s not in the local best block chain
     *
     * @var string
     */
    private $category;

    /**
     * A negative bitcoin amount if sending payment; a positive bitcoin amount if
     * receiving payment (including coinbases)
     *
     * @var float
     */
    private $amount;

    /**
     * For an output, the output index (vout) for this output in this transaction.
     * For an input, the output index for the output being spent in its transaction.
     * Because inputs list the output indexes from previous transactions,
     * more than one entry in the details array may have the same output index
     *
     * @var int
     */
    private $vout;

    /**
     * If sending payment, the fee paid as a negative bitcoins value. May be 0.
     * Not returned if receiving payment
     *
     * @var float|null
     */
    private $fee;

    /**
     * The number of confirmations the transaction has received. Will be 0 for unconfirmed and
     * -1 for conflicted
     *
     * @var int
     */
    private $confirmations;

    /**
     * Set to true if the transaction is a coinbase. Not returned for regular transactions
     *
     * @var bool|null
     */
    private $generated;

    /**
     * The hash of the block on the local best block chain which includes this transaction,
     * encoded as hex in RPC byte order. Only returned for confirmed transactions
     *
     * @var string|null
     */
    private $blockhash;

    /**
     * The index of the transaction in the block that includes it. Only returned for confirmed transactions
     *
     * @var int|null
     */
    private $blockindex;

    /**
     * The block header time (Unix epoch time) of the block on the local best block chain which
     * includes this transaction. Only returned for confirmed transactions
     *
     * @var int|null
     */
    private $blocktime;

    /**
     * The TXID of the transaction, encoded as hex in RPC byte order
     *
     * @var string
     */
    private $txid;

    /**
     * A Unix epoch time when the transaction was added to the wallet
     *
     * @var int
     */
    private $time;

    /**
     * A Unix epoch time when the transaction was detected by the local node,
     * or the time of the block on the local best block chain that included the transaction
     *
     * @var int
     */
    private $timereceived;

    /**
     * For transaction originating with this wallet, a locally-stored comment added to the transaction
     * identifying who the transaction was sent to. Only returned if a comment-to was added
     *
     * @var string|null
     */
    private $to;

    /**
     * The transaction in serialized transaction format
     *
     * @var string|null
     */
    private $hex;

    /**
     * @return bool|null
     */
    public function isInvolvesWatchonly(): ?bool
    {
        return $this->involvesWatchonly;
    }

    /**
     * @param bool|null $involvesWatchonly
     *
     * @return $this
     */
    public function setInvolvesWatchonly(?bool $involvesWatchonly): self
    {
        $this->involvesWatchonly = $involvesWatchonly;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccount(): string
    {
        return $this->account;
    }

    /**
     * @param string $account
     *
     * @return $this
     */
    public function setAccount(string $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param null|string $address
     *
     * @return $this
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     *
     * @return $this
     */
    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return int
     */
    public function getVout(): int
    {
        return $this->vout;
    }

    /**
     * @param int $vout
     *
     * @return $this
     */
    public function setVout(int $vout): self
    {
        $this->vout = $vout;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getFee(): ?float
    {
        return $this->fee;
    }

    /**
     * @param float|null $fee
     *
     * @return $this
     */
    public function setFee(?float $fee): self
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * @return int
     */
    public function getConfirmations(): int
    {
        return $this->confirmations;
    }

    /**
     * @param int $confirmations
     *
     * @return $this
     */
    public function setConfirmations(int $confirmations): self
    {
        $this->confirmations = $confirmations;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isGenerated(): ?bool
    {
        return $this->generated;
    }

    /**
     * @param bool|null $generated
     *
     * @return $this
     */
    public function setGenerated(?bool $generated): self
    {
        $this->generated = $generated;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getBlockhash(): ?string
    {
        return $this->blockhash;
    }

    /**
     * @param null|string $blockhash
     *
     * @return $this
     */
    public function setBlockhash(?string $blockhash): self
    {
        $this->blockhash = $blockhash;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getBlockindex(): ?int
    {
        return $this->blockindex;
    }

    /**
     * @param int|null $blockindex
     *
     * @return $this
     */
    public function setBlockindex(?int $blockindex): self
    {
        $this->blockindex = $blockindex;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getBlocktime(): ?int
    {
        return $this->blocktime;
    }

    /**
     * @param int|null $blocktime
     *
     * @return $this
     */
    public function setBlocktime(?int $blocktime): self
    {
        $this->blocktime = $blocktime;

        return $this;
    }

    /**
     * @return string
     */
    public function getTxid(): string
    {
        return $this->txid;
    }

    /**
     * @param string $txid
     *
     * @return $this
     */
    public function setTxid(string $txid): self
    {
        $this->txid = $txid;

        return $this;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimereceived(): int
    {
        return $this->timereceived;
    }

    /**
     * @param int $timereceived
     *
     * @return $this
     */
    public function setTimereceived(int $timereceived): self
    {
        $this->timereceived = $timereceived;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getTo(): ?string
    {
        return $this->to;
    }

    /**
     * @param null|string $to
     *
     * @return $this
     */
    public function setTo(?string $to): self
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getHex(): ?string
    {
        return $this->hex;
    }

    /**
     * @param null|string $hex
     *
     * @return $this
     */
    public function setHex(?string $hex): self
    {
        $this->hex = $hex;

        return $this;
    }
}
