<?php
/**
 * ListSinceBlockResult.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Result;

/**
 * An object containing an array of transactions and the lastblock field
 */
class ListSinceBlockResult
{
    /**
     * An array of objects each describing a particular payment to or from this wallet.
     * The objects in this array do not describe an actual transactions, so
     * more than one object in this array may come from the same transaction. This array may be empty
     *
     * @var Transaction[]
     */
    private $transactions;

    /**
     * The header hash of the block with the number of confirmations specified in the target confirmations parameter,
     * encoded as hex in RPC byte order
     *
     * @var string
     */
    private $lastblock;

    /**
     * @return Transaction[]
     */
    public function getTransactions(): array
    {
        return $this->transactions;
    }

    /**
     * @param Transaction[] $transactions
     *
     * @return $this
     */
    public function setTransactions(array $transactions): self
    {
        $this->transactions = $transactions;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastblock(): string
    {
        return $this->lastblock;
    }

    /**
     * @param string $lastblock
     *
     * @return $this
     */
    public function setLastblock(string $lastblock): self
    {
        $this->lastblock = $lastblock;

        return $this;
    }
}
