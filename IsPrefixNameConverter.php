<?php
/**
 * IsPrefixNameConverter.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit;

use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

/**
 * 'is' prefix.
 */
class IsPrefixNameConverter implements NameConverterInterface
{
    /**
     * {@inheritDoc}
     */
    public function normalize($propertyName)
    {
        return $propertyName;
    }

    /**
     * {@inheritDoc}
     */
    public function denormalize($propertyName)
    {
        return preg_match('/^is/', $propertyName) ? substr($propertyName, 2) : $propertyName;
    }
}
