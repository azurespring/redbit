<?php
/**
 * RedbitInterface.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit;

/**
 * Reduced bitcoin interface.
 */
interface RedbitInterface
{
    const IGHASHTYPE_ALL = 'ALL';
    const IGHASHTYPE_NONE = 'NONE';
    const IGHASHTYPE_SINGLE = 'SINGLE';
    const IGHASHTYPE_ALL_ANYONECANPAY = 'ALL|ANYONECANPAY';
    const IGHASHTYPE_NONE_ANYONECANPAY = 'NONE|ANYONECANPAY';
    const IGHASHTYPE_SINGLE_ANYONECANPAY = 'SINGLE|ANYONECANPAY';

    const SATOSHI = 1e-8;
    const DUST_THRESHOLD = 546; /* satoshis */

    //! Standard JSON-RPC 2.0 errors
    const RPC_INVALID_REQUEST  = -32600;
    const RPC_METHOD_NOT_FOUND = -32601;
    const RPC_INVALID_PARAMS   = -32602;
    const RPC_INTERNAL_ERROR   = -32603;
    const RPC_PARSE_ERROR      = -32700;

    //! General application defined errors
    const RPC_MISC_ERROR                  = -1;  //! std::exception thrown in command handling
    const RPC_FORBIDDEN_BY_SAFE_MODE      = -2;  //! Server is in safe mode, and command is not allowed in safe mode
    const RPC_TYPE_ERROR                  = -3;  //! Unexpected type was passed as parameter
    const RPC_INVALID_ADDRESS_OR_KEY      = -5;  //! Invalid address or key
    const RPC_OUT_OF_MEMORY               = -7;  //! Ran out of memory during operation
    const RPC_INVALID_PARAMETER           = -8;  //! Invalid, missing or duplicate parameter
    const RPC_DATABASE_ERROR              = -20; //! Database error
    const RPC_DESERIALIZATION_ERROR       = -22; //! Error parsing or validating structure in raw format
    const RPC_VERIFY_ERROR                = -25; //! General error during transaction or block submission
    const RPC_VERIFY_REJECTED             = -26; //! Transaction or block was rejected by network rules
    const RPC_VERIFY_ALREADY_IN_CHAIN     = -27; //! Transaction already in chain
    const RPC_IN_WARMUP                   = -28; //! Client still warming up

    //! Aliases for backward compatibility
    const RPC_TRANSACTION_ERROR           = self::RPC_VERIFY_ERROR;
    const RPC_TRANSACTION_REJECTED        = self::RPC_VERIFY_REJECTED;
    const RPC_TRANSACTION_ALREADY_IN_CHAIN = self::RPC_VERIFY_ALREADY_IN_CHAIN;

    //! P2P client errors
    const RPC_CLIENT_NOT_CONNECTED        = -9;  //! Bitcoin is not connected
    const RPC_CLIENT_IN_INITIAL_DOWNLOAD  = -10; //! Still downloading initial blocks
    const RPC_CLIENT_NODE_ALREADY_ADDED   = -23; //! Node is already added
    const RPC_CLIENT_NODE_NOT_ADDED       = -24; //! Node has not been added before

    //! Wallet errors
    const RPC_WALLET_ERROR                = -4;  //! Unspecified problem with wallet (key not found etc.)
    const RPC_WALLET_INSUFFICIENT_FUNDS   = -6;  //! Not enough funds in wallet or account
    const RPC_WALLET_INVALID_ACCOUNT_NAME = -11; //! Invalid account name
    const RPC_WALLET_KEYPOOL_RAN_OUT      = -12; //! Keypool ran out, call keypoolrefill first
    const RPC_WALLET_UNLOCK_NEEDED        = -13; //! Enter the wallet passphrase with walletpassphrase first
    const RPC_WALLET_PASSPHRASE_INCORRECT = -14; //! The wallet passphrase entered was incorrect
    const RPC_WALLET_WRONG_ENC_STATE      = -15; //! Command given in wrong wallet encryption state (encrypting an encrypted wallet etc.)
    const RPC_WALLET_ENCRYPTION_FAILED    = -16; //! Failed to encrypt the wallet
    const RPC_WALLET_ALREADY_UNLOCKED     = -17; //! Wallet is already unlocked

    /**
     * The walletlock RPC removes the wallet encryption key from memory, locking the wallet.
     * After calling this method, you will need to call walletpassphrase again before
     * being able to call any methods which require the wallet to be unlocked.
     *
     * https://bitcoin.org/en/developer-reference#walletlock
     *
     * @return $this
     */
    public function walletLock();

    /**
     * The walletpassphrase RPC stores the wallet decryption key in memory for
     * the indicated number of seconds. Issuing the walletpassphrase command while
     * the wallet is already unlocked will set a new unlock time that overrides the old one.
     *
     * @param string $passphrase The passphrase that unlocks the wallet
     * @param int    $timeout    The number of seconds after which the decryption key will
     *                           be automatically deleted from memory
     *
     * @return $this
     */
    public function walletPassphrase(string $passphrase, int $timeout);

    /**
     * The validateaddress RPC returns information about the given Bitcoin address.
     *
     * https://bitcoin.org/en/developer-reference#validateaddress
     *
     * @param string $address The P2PKH or P2SH address to validate encoded in base58check format
     *
     * @return Result\ValidateAddressResult
     */
    public function validateAddress(string $address): Result\ValidateAddressResult;

    /**
     * The listsinceblock RPC gets all transactions affecting the wallet which have occurred since a particular block,
     * plus the header hash of a block at a particular depth.
     *
     * https://bitcoin.org/en/developer-reference#listsinceblock
     *
     * @param string|null $hash          The hash of a block header encoded as hex in RPC byte order.
     *                                   All transactions affecting the wallet which are not in that block or
     *                                   any earlier block will be returned, including unconfirmed transactions.
     *                                   Default is the hash of the genesis block, so all transactions
     *                                   affecting the wallet are returned by default
     * @param int         $confirmations Sets the lastblock field of the results to the header hash of a block
     *                                   with this many confirmations. This does not affect which transactions are returned.
     *                                   Default is 1, so the hash of the most recent block on the local best block chain is returned
     * @param bool        $watchOnly     If set to true, include watch-only addresses in details and
     *                                   calculations as if they were regular addresses belonging to the wallet.
     *                                   If set to false (the default), treat watch-only addresses
     *                                   as if they didn’t belong to this wallet
     *
     * @return Result\ListSinceBlockResult
     */
    public function listSinceBlock(string $hash = '', int $confirmations = 1, bool $watchOnly = false): Result\ListSinceBlockResult;

    /**
     * The gettransaction RPC gets detailed information about an in-wallet transaction.
     *
     * https://bitcoin.org/en/developer-reference#gettransaction
     *
     * @param string $txId The TXID of the transaction to get details about.
     *                     The TXID must be encoded as hex in RPC byte order
     *
     * @return Result\Transaction
     */
    public function getTransaction(string $txId): Result\Transaction;

    /**
     * The getbalance RPC gets the balance in decimal bitcoins across all accounts or
     * for a particular account.
     *
     * https://bitcoin.org/en/developer-reference#getbalance
     *
     * @param string $account The name of an account to get the balance for.
     *                        An empty string (“”) is the default account.
     *                        The string * will get the balance for all accounts (this is the default behavior)
     *
     * @return string The balance of the account (or all accounts) in bitcoins
     */
    public function getBalance(string $account = '*'): string;

    /**
     * The listunspent RPC returns an array of unspent transaction outputs belonging to this wallet.
     * Note: as of Bitcoin Core 0.10.0, outputs affecting watch-only addresses will be returned;
     * see the spendable field in the results described below.
     *
     * https://bitcoin.org/en/developer-reference#listunspent
     *
     * @param int           $minconf   The minimum number of confirmations the transaction containing an output must have
     *                                 in order to be returned. Use 0 to return outputs from unconfirmed transactions.
     * @param int           $maxconf   The maximum number of confirmations the transaction containing an output may have
     *                                 in order to be returned.
     * @param string[]|null $addresses If present, only outputs which pay an address in this array will be returned
     *
     * @return Result\UnspentOutput[]
     */
    public function listUnspent(int $minconf = 1, int $maxconf = 9999999, ?array $addresses = null): array;

    /**
     * The estimatefee RPC estimates the transaction fee per kilobyte that needs to
     * be paid for a transaction to be included within a certain number of blocks.
     *
     * https://bitcoin.org/en/developer-reference#estimatefee
     *
     * @param int $blocks The maximum number of blocks a transaction should have to wait before
     *                    it is predicted to be included in a block. Has to be between 2 and 25 blocks
     *
     * @return float
     */
    public function estimateFee(int $blocks): float;

    /**
     * The getnewaddress RPC returns a new Bitcoin address for receiving payments.
     * If an account is specified, payments received with the address will be credited to that account.
     *
     * https://bitcoin.org/en/developer-reference#getnewaddress
     *
     * @param string $account The name of the account to put the address in.
     *                        The default is the default account, an empty string (“”)
     *
     * @return string A P2PKH address which has not previously been returned by this RPC.
     */
    public function getNewAddress(string $account = ''): string;

    /**
     * The dumpprivkey RPC returns the wallet-import-format (WIF) private key corresponding to an address.
     * (But does not remove it from the wallet.)
     *
     * https://bitcoin.org/en/developer-reference#dumpprivkey
     *
     * @param string $address
     *
     * @return string
     */
    public function dumpPrivKey(string $address): string;

    /**
     * The importprivkey RPC adds a private key to your wallet. The key should be
     * formatted in the wallet import format created by the dumpprivkey RPC.
     *
     * @param string $privKey The private key to import into the wallet
     *                        encoded in base58check using wallet import format (WIF)
     * @param string $account The name of an account to which transactions involving the key should be assigned.
     *                        The default is the default account, an empty string (“”)
     * @param bool   $rescan  Set to true (the default) to rescan the entire local block database for
     *                        transactions affecting any address or pubkey script in the wallet
     *                        (including transaction affecting the newly-added address for this private key).
     *                        Set to false to not rescan the block database (rescanning can be
     *                        performed at any time by restarting Bitcoin Core with the -rescan command-line argument).
     *                        Rescanning may take several minutes.
     *                        Notes: if the address for this key is already in the wallet, the block database will not
     *                        be rescanned even if this parameter is set
     *
     * @return $this
     */
    public function importPrivKey(string $privKey, string $account = '', bool $rescan = true);

    /**
     * The createrawtransaction RPC creates an unsigned serialized transaction that
     * spends a previous output to a new output with a P2PKH or P2SH address.
     * The transaction is not stored in the wallet or transmitted to the network.
     *
     * https://bitcoin.org/en/developer-reference#createrawtransaction
     *
     * @param Parameter\Input[] $inputs   An array of objects, each one to be used as an input to the transaction
     * @param array             $outputs  A key/value pair with the address to pay as a string (key) and
     *                                    the amount to pay that address (value) in bitcoins
     * @param int|null          $locktime Indicates the earliest time a transaction can be added to the block chain
     *
     * @return string
     */
    public function createRawTransaction(array $inputs, array $outputs, int $locktime = null): string;

    /**
     * The fundrawtransaction RPC adds inputs to a transaction until it has enough in value to meet its out value.
     * This will not modify existing inputs, and will add one change output to the outputs.
     * Note that inputs which were signed may need to be resigned after completion since in/outputs have been added.
     * The inputs added will not be signed, use signrawtransaction for that.
     * All existing inputs must have their previous output transaction be in the wallet.
     *
     * https://bitcoin.org/en/developer-reference#fundrawtransaction
     *
     * @param string                                   $hex
     * @param Parameter\FundRawTransactionOptions|null $options
     *
     * @return Result\FundRawTransactionResult
     */
    public function fundRawTransaction(string $hex, ?Parameter\FundRawTransactionOptions $options = null): Result\FundRawTransactionResult;

    /**
     * The signrawtransaction RPC signs a transaction in the serialized transaction format
     * using private keys stored in the wallet or provided in the call.
     *
     * https://bitcoin.org/en/developer-reference#signrawtransaction
     *
     * @param string                                    $hex
     * @param Parameter\UnspentTransactionOutput[]|null $prevTxs
     * @param array|null                                $privKeys
     * @param string|null                               $sighashtype
     *
     * @return Result\SignRawTransactionResult
     */
    public function signRawTransaction(string $hex, ?array $prevTxs = null, ?array $privKeys = null, ?string $sighashtype = null): Result\SignRawTransactionResult;

    /**
     * The sendrawtransaction RPC validates a transaction and broadcasts it to the peer-to-peer network.
     *
     * https://bitcoin.org/en/developer-reference#sendrawtransaction
     *
     * @param string $transaction   The serialized transaction to broadcast encoded as hex
     * @param bool   $allowHighFees Set to true to allow the transaction to pay a high transaction fee.
     *                              Set to false (the default) to prevent Bitcoin Core from
     *                              broadcasting the transaction if it includes a high fee.
     *                              Transaction fees are the sum of the inputs minus the sum of the outputs,
     *                              so this high fees check helps ensures user including a change address to
     *                              return most of the difference back to themselves
     *
     * @return string
     */
    public function sendRawTransaction(string $transaction, bool $allowHighFees = false): string;

    /**
     * The decoderawtransaction RPC decodes a serialized transaction hex string into
     * a JSON object describing the transaction.
     *
     * https://bitcoin.org/en/developer-reference#decoderawtransaction
     *
     * @param string $transaction
     *
     * @return Result\DecodeRawTransactionResult
     */
    public function decodeRawTransaction(string $transaction): Result\DecodeRawTransactionResult;
}
