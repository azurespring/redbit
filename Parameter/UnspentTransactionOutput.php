<?php
/**
 * UnspentTransactionOutput.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Parameter;

/**
 */
class UnspentTransactionOutput implements \JsonSerializable
{
    /**
     * The TXID of the transaction the output appeared in.
     * The TXID must be encoded in hex in RPC byte order
     *
     * @var string
     */
    private $txid;

    /**
     * The index number of the output (vout) as it appeared in its transaction,
     * with the first output being 0
     *
     * @var int
     */
    private $vout;

    /**
     * The output’s pubkey script encoded as hex
     *
     * @var string
     */
    private $scriptPubKey;

    /**
     * If the pubkey script was a script hash, this must be the corresponding redeem script
     *
     * @var string|null
     */
    private $redeemScript;

    /**
     * Constructor.
     *
     * @param string      $txid
     * @param int         $vout
     * @param string      $scriptPubKey
     * @param string|null $redeemScript
     */
    public function __construct(string $txid, int $vout, string $scriptPubKey, ?string $redeemScript = null)
    {
        $this->txid = $txid;
        $this->vout = $vout;
        $this->scriptPubKey = $scriptPubKey;
        $this->redeemScript = $redeemScript;
    }

    /**
     * @return string
     */
    public function getTxid(): string
    {
        return $this->txid;
    }

    /**
     * @return int
     */
    public function getVout(): int
    {
        return $this->vout;
    }

    /**
     * @return string
     */
    public function getScriptPubKey(): string
    {
        return $this->scriptPubKey;
    }

    /**
     * @return null|string
     */
    public function getRedeemScript(): ?string
    {
        return $this->redeemScript;
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return [
            'txid' => $this->getTxid(),
            'vout' => $this->getVout(),
            'scriptPubKey' => $this->getScriptPubKey(),
            'redeemScript' => $this->getRedeemScript(),
        ];
    }
}
