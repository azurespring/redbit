<?php
/**
 * Input.php
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Parameter;

/**
 * An object describing a particular input
 */
class Input implements \JsonSerializable
{
    /**
     * @var string
     */
    private $txid;

    /**
     * @var int
     */
    private $vout;

    /**
     * @var int|null
     */
    private $sequence;

    /**
     * Constructor.
     *
     * @param string   $txid
     * @param int      $vout
     * @param int|null $sequence
     */
    public function __construct(string $txid, int $vout, ?int $sequence = null)
    {
        $this->txid = $txid;
        $this->vout = $vout;
        $this->sequence = $sequence;
    }

    /**
     * @return string
     */
    public function getTxid(): string
    {
        return $this->txid;
    }

    /**
     * @return int
     */
    public function getVout(): int
    {
        return $this->vout;
    }

    /**
     * @return int|null
     */
    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return [
            'txid' => $this->getTxid(),
            'vout' => $this->getVout(),
            'sequence' => $this->getSequence(),
        ];
    }
}
