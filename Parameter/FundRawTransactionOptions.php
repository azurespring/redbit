<?php
/**
 * FundRawTransactionOptions
 *
 * @author tsai phan <phanalpha@hotmail.com>
 */
namespace AzureSpring\Redbit\Parameter;

/**
 * Additional options
 */
class FundRawTransactionOptions implements \JsonSerializable
{
    /**
     * @var string|null
     */
    private $changeAddress;

    /**
     * @var int|null
     */
    private $changePosition;

    /**
     * @var bool|null
     */
    private $includeWatching;

    /**
     * @var bool|null
     */
    private $lockUnspents;

    /**
     * @var float|null
     */
    private $feeRate;

    /**
     * @var int[]|null
     */
    private $subtractFeeFromOutputs;

    /**
     * @return null|string
     */
    public function getChangeAddress(): ?string
    {
        return $this->changeAddress;
    }

    /**
     * @param null|string $changeAddress
     *
     * @return $this
     */
    public function setChangeAddress(?string $changeAddress): self
    {
        $this->changeAddress = $changeAddress;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getChangePosition(): ?int
    {
        return $this->changePosition;
    }

    /**
     * @param int|null $changePosition
     *
     * @return $this
     */
    public function setChangePosition(?int $changePosition): self
    {
        $this->changePosition = $changePosition;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIncludeWatching(): ?bool
    {
        return $this->includeWatching;
    }

    /**
     * @param bool|null $includeWatching
     *
     * @return $this
     */
    public function setIncludeWatching(?bool $includeWatching): self
    {
        $this->includeWatching = $includeWatching;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getLockUnspents(): ?bool
    {
        return $this->lockUnspents;
    }

    /**
     * @param bool|null $lockUnspents
     *
     * @return $this
     */
    public function setLockUnspents(?bool $lockUnspents): self
    {
        $this->lockUnspents = $lockUnspents;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getFeeRate(): ?float
    {
        return $this->feeRate;
    }

    /**
     * @param float|null $feeRate
     *
     * @return $this
     */
    public function setFeeRate(?float $feeRate): self
    {
        $this->feeRate = $feeRate;

        return $this;
    }

    /**
     * @return int[]|null
     */
    public function getSubtractFeeFromOutputs(): ?array
    {
        return $this->subtractFeeFromOutputs;
    }

    /**
     * @param int[]|null $subtractFeeFromOutputs
     *
     * @return $this
     */
    public function setSubtractFeeFromOutputs(?array $subtractFeeFromOutputs): self
    {
        $this->subtractFeeFromOutputs = $subtractFeeFromOutputs;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return (object) array_filter(
            [
                'changeAddress'   => $this->getChangeAddress(),
                'changePosition'  => $this->getChangePosition(),
                'includeWatching' => $this->getIncludeWatching(),
                'lockUnspents'    => $this->getLockUnspents(),
                'feeRate'         => $this->getFeeRate(),
            ],
            function ($var) {
                return null !== $var;
            }
        );
    }
}
